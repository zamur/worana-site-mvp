import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LayoutMainComponent } from "./layout-main/layout-main.component";

const routes: Routes = [
  {
    path: "",
    component: LayoutMainComponent,
    resolve: {},
    children: [
      {
        path: "",
        loadChildren:
          "../landing/landing.module#LandingModule"
      },
      {
        path: "acessar",
        loadChildren:
          "../acessar/acessar.module#AcessarModule"
      },
      {
        path: "perfil",
        loadChildren:
          "../perfil/perfil.module#PerfilModule"
      },
      {
        path: "agendamento",
        loadChildren:
          "../agendamento/agendamento.module#AgendamentoModule"
      },
      {
        path: "pagamento",
        loadChildren:
          "../pagamento/pagamento.module#PagamentoModule"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
