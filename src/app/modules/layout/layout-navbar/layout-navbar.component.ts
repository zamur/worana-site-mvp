import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-layout-navbar',
  templateUrl: './layout-navbar.component.html',
  styleUrls: ['./layout-navbar.component.scss']
})
export class LayoutNavbarComponent implements OnInit {

  constructor(private router: Router, @Inject(DOCUMENT) private document: any) { }

  ngOnInit() {
  }

}
