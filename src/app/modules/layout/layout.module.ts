import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LayoutRoutingModule } from "./layout-routing.module";
import { LayoutMainComponent } from "./layout-main/layout-main.component";
import { LayoutFooterComponent } from "./layout-footer/layout-footer.component";
import { LayoutNavbarComponent } from "./layout-navbar/layout-navbar.component";

import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [
    LayoutMainComponent,
    LayoutFooterComponent,
    LayoutNavbarComponent
  ],
  imports: [CommonModule, SharedModule, LayoutRoutingModule]
})
export class LayoutModule {}
