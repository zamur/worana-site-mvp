import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgendamentoRoutingModule } from './agendamento-routing.module';
import { AgendamentoMainComponent } from './agendamento-main/agendamento-main.component';
import { ResultadosComponent } from './resultados/resultados.component';
import { SharedModule } from '../shared/shared.module';
import { PagamentoModule } from '../pagamento/pagamento.module';

@NgModule({
  declarations: [AgendamentoMainComponent, ResultadosComponent],
  imports: [
    CommonModule,
    AgendamentoRoutingModule,
    SharedModule,
    PagamentoModule
  ]
})
export class AgendamentoModule { }
