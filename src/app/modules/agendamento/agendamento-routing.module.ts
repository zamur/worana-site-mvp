import { AgendamentoMainComponent } from './agendamento-main/agendamento-main.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultadosComponent } from './resultados/resultados.component';
import { PagamentoDadosComponent } from '../pagamento/pagamento-dados/pagamento-dados.component';

const routes: Routes = [
  {
    path: "",
    component: AgendamentoMainComponent,
    children: [
      {
        path: "",
        redirectTo: "resultados",
        pathMatch: "full"
      },
      {
        path: "resultados",
        component: ResultadosComponent
      },
      {
        path: "pagamento",
        component: PagamentoDadosComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgendamentoRoutingModule { }