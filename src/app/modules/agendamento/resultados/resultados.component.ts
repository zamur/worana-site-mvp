import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';


export interface StateGroup {
  tipo: string;
  names: string[];
}

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.scss']
})
export class ResultadosComponent implements OnInit {
  stateForm: FormGroup = this.fb.group({
    stateGroup: '',
  });

  stateGroups: StateGroup[] = [
    {
      tipo: 'Especialidade',
      names: ['Cardiologia', 'Dermatologia', 'Obstetria', 'Pediatria']
    }, {
      tipo: 'Sintoma',
      names: ['Coceira', 'Dermatite', 'Dor de cabeça', 'Torção de joelho']
    }
  ];

  evento = "Casamento";
  local = "Belo Horizonte";
  value;

  favoriteSeason: string;
  seasons: string[] = ['Ótimo', 'Muito Bom', 'Bom'];

  stateGroupOptions: Observable<StateGroup[]>;
  mostrarOutros = false;

  constructor(private router: Router, private route: ActivatedRoute, private fb: FormBuilder) { }

  ngOnInit() {

    this.stateGroupOptions = this.stateForm.get('stateGroup')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
  }

  mostrarOutrosResultados() {
    this.mostrarOutros = true;
  }

  onAgendar() {
    this.router.navigate(['/agendamento']);
  }

  goToPagamento() {
    this.router.navigateByUrl('/pagamento');
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      return this.stateGroups
        .map(group => ({ tipo: group.tipo, names: _filter(group.names, value) }))
        .filter(group => group.names.length > 0);
    }
    return this.stateGroups;
  }
}
