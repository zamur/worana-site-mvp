import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendamentoMainComponent } from './agendamento-main.component';

describe('AgendamentoMainComponent', () => {
  let component: AgendamentoMainComponent;
  let fixture: ComponentFixture<AgendamentoMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendamentoMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendamentoMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
