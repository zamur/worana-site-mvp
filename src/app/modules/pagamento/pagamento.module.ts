import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagamentoRoutingModule } from './pagamento-routing.module';
import { PagamentoMainComponent } from './pagamento-main/pagamento-main.component';
import { PagamentoDadosComponent } from './pagamento-dados/pagamento-dados.component';
import { PagamentoConfirmacaoComponent } from './pagamento-confirmacao/pagamento-confirmacao.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PagamentoMainComponent, PagamentoDadosComponent, PagamentoConfirmacaoComponent],
  imports: [
    CommonModule,
    PagamentoRoutingModule,
    SharedModule
  ]
})
export class PagamentoModule { }
