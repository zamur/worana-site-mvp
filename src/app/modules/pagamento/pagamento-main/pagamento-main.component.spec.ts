import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagamentoMainComponent } from './pagamento-main.component';

describe('PagamentoMainComponent', () => {
  let component: PagamentoMainComponent;
  let fixture: ComponentFixture<PagamentoMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagamentoMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagamentoMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
