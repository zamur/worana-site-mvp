import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagamentoConfirmacaoComponent } from './pagamento-confirmacao.component';

describe('PagamentoConfirmacaoComponent', () => {
  let component: PagamentoConfirmacaoComponent;
  let fixture: ComponentFixture<PagamentoConfirmacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagamentoConfirmacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagamentoConfirmacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
