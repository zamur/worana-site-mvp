import { PagamentoDadosComponent } from './pagamento-dados/pagamento-dados.component';
import { PagamentoMainComponent } from './pagamento-main/pagamento-main.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagamentoConfirmacaoComponent } from './pagamento-confirmacao/pagamento-confirmacao.component';

const routes: Routes = [
  {
    path: "",
    component: PagamentoMainComponent,
    children: [
      {
        path: "",
        redirectTo: "pagamento-dados",
        pathMatch: "full"
      },
      {
        path: "pagamento-dados",
        component: PagamentoDadosComponent
      },
      {
        path: "confirmacao",
        component: PagamentoConfirmacaoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagamentoRoutingModule { }