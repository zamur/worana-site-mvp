import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagamentoDadosComponent } from './pagamento-dados.component';

describe('PagamentoDadosComponent', () => {
  let component: PagamentoDadosComponent;
  let fixture: ComponentFixture<PagamentoDadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagamentoDadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagamentoDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
