import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pagamento-dados',
  templateUrl: './pagamento-dados.component.html',
  styleUrls: ['./pagamento-dados.component.scss']
})
export class PagamentoDadosComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToConfirmacao() {
    this.router.navigateByUrl('/pagamento/confirmacao');
  }

}
