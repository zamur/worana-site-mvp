import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { AcessarRoutingModule } from './acessar-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [MainComponent, LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    AcessarRoutingModule,
    SharedModule
  ]
})
export class AcessarModule { }
