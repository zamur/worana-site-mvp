import { MaterialModule } from './material.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [CommonModule],
  exports: [ReactiveFormsModule, FormsModule, MaterialModule],
  providers: []
})
export class SharedModule { }
