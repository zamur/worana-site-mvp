import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { LandingRoutingModule } from './landing-routing.module';
import { MainComponent } from './main/main.component';
import { IntroComponent } from './intro/intro.component';
import { EntradaComponent } from './intro/entrada/entrada.component';
import { DiferenciaisComponent } from './intro/diferenciais/diferenciais.component';
import { ComoFuncionaComponent } from './intro/como-funciona/como-funciona.component';
import { FaqComponent } from './intro/faq/faq.component';
import { MelhoresAvaliacoesComponent } from './intro/melhores-avaliacoes/melhores-avaliacoes.component';

@NgModule({
  declarations:  [MainComponent, IntroComponent, EntradaComponent, DiferenciaisComponent, ComoFuncionaComponent, FaqComponent, MelhoresAvaliacoesComponent],
  imports: [
    CommonModule,
    SharedModule,
    LandingRoutingModule
  ]
})
export class LandingModule { }
