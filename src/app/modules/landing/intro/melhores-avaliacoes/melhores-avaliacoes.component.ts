import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

export interface StateGroup {
  tipo: string;
  names: string[];
}
export interface Local {
  value: string;
  viewValue: string;
}

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'app-melhores-avaliacoes',
  templateUrl: './melhores-avaliacoes.component.html',
  styleUrls: ['./melhores-avaliacoes.component.scss']
})
export class MelhoresAvaliacoesComponent implements OnInit {
  myForm: FormGroup = this.fb.group({
    evento: '',
    local: ""
  });

  evento = "Casamento";
  local = "Belo Horizonte";

  stateGroups: StateGroup[] = [
    {
      tipo: 'Social',
      names: ['Casamento', 'Debutante', 'Bodas', 'Aniversário']
    }, {
      tipo: 'Ensaio',
      names: ['Sensual', 'Newborn', 'Book', 'Casamento']
    }
  ];

  locais: Local[] = [
    { value: '1', viewValue: 'Belo Horizonte' },
    { value: '2', viewValue: 'São Paulo' },
    { value: '3', viewValue: 'Rio de Janeiro' }
  ];

  stateGroupOptions: Observable<StateGroup[]>;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.stateGroupOptions = this.myForm.get('evento')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );

    this.stateGroupOptions = this.myForm.get('local')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
  }

  private _filterGroup(value: string): StateGroup[] {
    if (value) {
      return this.stateGroups
        .map(group => ({ tipo: group.tipo, names: _filter(group.names, value) }))
        .filter(group => group.names.length > 0);
    }

    return this.stateGroups;
  }
}
