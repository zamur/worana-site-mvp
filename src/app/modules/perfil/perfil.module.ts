import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { MainComponent } from './main/main.component';
import { PerfilUserComponent } from './perfil-user/perfil-user.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [MainComponent, PerfilUserComponent],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    SharedModule
  ]
})
export class PerfilModule { }
